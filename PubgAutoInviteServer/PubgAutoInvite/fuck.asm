EXTERN g_dwJmp:DQ;
EXTERN g_strProtobuf:DQ;
EXTERN g_dwlen:DQ;
EXTERN CheckData:PROC;

.CODE


PUSHAQ MACRO
	push	rcx
    push    rax
    push    rdx
    push    rbx
    push    rbp
    push    rsi
    push    rdi
    push    r8
    push    r9
    push    r10
    push    r11
    push    r12
    push    r13
    push    r14
    push    r15
ENDM


POPAQ MACRO
    pop    r15
    pop    r14
    pop    r13
    pop    r12
    pop    r11
    pop    r10
    pop    r9
    pop    r8
    pop    rdi
    pop    rsi
    pop    rbp
    pop    rbx
    pop    rdx
    pop    rax
	pop    rcx
ENDM





hookSend PROC

	


 PUSHAQ
 pushfq


  mov  rcx,r8

  call CheckData
  test al, al
  je  exit
  popfq
  POPAQ
  lea r8,g_strProtobuf
  mov r9,g_dwlen
  mov [rax-58h],r9
  mov rsi,rcx
  mov [rax-60h],r8
  lea rcx,[rax-30h]
  jmp g_dwJmp


exit:
 popfq
 POPAQ
 mov [rax-58h],r9
 mov rsi,rcx
 mov [rax-60h],r8
 lea rcx,[rax-30h]


 jmp g_dwJmp

hookSend ENDP

END
