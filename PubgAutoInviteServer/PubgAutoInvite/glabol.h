#pragma once
#include "stdafx.h"
#include "windows.h"
#include "stdio.h"
#include "ShareMemory.hpp"

#include <websocketpp/config/asio_no_tls.hpp>  
#include <websocketpp/server.hpp>
#include <functional>
#include <string>
#include <openssl/aes.h>
#include <document.h>
#include <stringbuffer.h>
#include <writer.h>
#include "winhttp.h"
#include "PSAPI.H"
#include "tlhelp32.h"
#include "shlwapi.h"

#pragma comment( lib, "PSAPI.LIB" )
#pragma comment( lib, "shlwapi.LIB" )
extern "C" DWORD64  g_dwJmp;

extern "C" char g_strProtobuf[1000];
extern "C" DWORD64 g_dwlen;

template<int LEN = 256>
class CFormatStringA
{
public:
	CFormatStringA(const char *fmt, ...)
	{
		va_list argptr;
		va_start(argptr, fmt);
		vsprintf_s(buf, fmt, argptr);
		va_end(argptr);
	}
	~CFormatStringA()
	{
#ifndef _DEBUG
		memset(buf, 0, sizeof(buf));
#endif
	}
	const char *get() const
	{
		return buf;
	}
	char buf[LEN];
};